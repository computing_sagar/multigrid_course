//
//  main.cpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include <iostream>
#include "Grid.hpp"
#include "VcycleFormation.hpp"
#include "Stencil.hpp"
#include "MultigridSolver.hpp"
#include "Time.hpp"
#include "Configuration.h"
#include <stdlib.h>
using namespace SIWIR2::MULTIGRID ;

int main(int argc,char * argv[]) {

    // Configuring the input parameter
    Configuration config(argc,argv) ;
    if(config.checkParameters()==0){

        std::cout<<"INVALID INPUT"<<std::endl ;
        exit(EXIT_FAILURE) ;
    }

    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout<<std::endl;

    std::cout<<"Starting Solver...."<<std::endl ;

    // Initialising the Solver Object
    SIWIR2::MULTIGRID::Solver s(config.FinestLevel(),
                                config.NumberOfPreSmoothners(),
                                config.NumberOfPostSmoothners(),
                                config.numberOfVcycles(),
                                config.boundaryConditions()) ;

    // Starting the Simulation
    s.start_Simulation();

    // Finally Writing the solution
    s.write_Solution(config.FinestLevel(),GridType::Ugrid) ;
    return 0;
}

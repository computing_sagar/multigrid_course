
#include "Configuration.h"

SIWIR2::MULTIGRID::Configuration::Configuration(int argc,char *argv[]) : finestLevel_(std::stoi(std::string((argv[1])))),
                                                                         numberOfVcycles_(std::stoi(std::string((argv[2])))){


    if(argc ==3){
        std::cout<<"...........Taking Default Boundary Conditions..........."<<std::endl ;
        boundaryConditions_ = "D" ;}
    else {
        boundaryConditions_ = argv[3] ;
    }
    numberOfPreSmoothners_ = 2 ;
    numberOfPostSmoothners_= 1 ;

    std::cout<<"................CONFIGURATION DONE"<<std::endl ;
}

SIWIR2::MULTIGRID::Configuration::Configuration() {

    finestLevel_ = 8 ;
    numberOfPreSmoothners_ = 2 ;
    numberOfPostSmoothners_= 1 ;
    numberOfVcycles_ = 6 ;
    boundaryConditions_ = "D" ;

}

SIWIR2::MULTIGRID::Configuration::~Configuration() {

}

bool SIWIR2::MULTIGRID::Configuration::checkParameters(){

    std::cout<<"CHECKING BOUNDARY CONDITIONS..FOR NEGATIVE ENTRIES"<<std::endl ;
    std::cout<<"CONVERTING DECIMAL ENTRIES TO UNSIGNED ENTRIES "<<std::endl;
    return (!(std::signbit(finestLevel_) || std::signbit(numberOfPreSmoothners_)
            || std::signbit(numberOfPostSmoothners_) || std::signbit(numberOfVcycles_))) ;

}

real_l SIWIR2::MULTIGRID::Configuration::FinestLevel() {

    return static_cast<real_l>(finestLevel_) ;
}

real_l SIWIR2::MULTIGRID::Configuration::NumberOfPreSmoothners() {

    return static_cast<real_l>(numberOfPreSmoothners_) ;
}

real_l SIWIR2::MULTIGRID::Configuration::NumberOfPostSmoothners() {

    return static_cast<real_l>(numberOfPostSmoothners_) ;
}

real_l SIWIR2::MULTIGRID::Configuration::numberOfVcycles(){

    return static_cast<real_l>(numberOfVcycles_) ;
}

BOUNDARY SIWIR2::MULTIGRID::Configuration::boundaryConditions(){

    std::cout<<"CHECKING VALIDITY FOR BOUNDARY CONDITIONS"<<std::endl;
    if (boundaryConditions_ == "D")
        return BOUNDARY::Dirichlet  ;
    else if (boundaryConditions_ == "N")
        return BOUNDARY::Neumann ;
    else
        std::cout<<"SOMETHING WRONG , SWITCHING TO DIRICHLET BOUNDARY CONDITIONS "<<std::endl ;
        return BOUNDARY::Dirichlet ;
}

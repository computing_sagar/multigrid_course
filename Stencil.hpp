//
//  Stencil.hpp
//  Assignment_1
//
//  Created by Sagar Dolas on 26/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Stencil_hpp
#define Stencil_hpp

#include <stdio.h>
#include <iostream>
#include "Types.h"
namespace SIWIR2 {
    namespace MULTIGRID{
    
    class Stencil {
        
    public:
        
        constexpr static const real_d C = 4 ; // Changin for the multigrid COURSE
        constexpr static const real_d N = 1 ;
        constexpr static const real_d S = 1 ;
        constexpr static const real_d E = 1 ;
        constexpr static const real_d W = 1 ;
        
        Stencil(){} ;
        ~Stencil(){} ;
    };

    class NinePointStencil {

        constexpr static const real_d C = 8.0 / 3 ;
        constexpr static const real_d N = 1.0 / 3 ;
        constexpr static const real_d S = 1.0 / 3 ;
        constexpr static const real_d E = 1.0 / 3 ;
        constexpr static const real_d W = 1.0 / 3 ;
        constexpr static const real_d NW = 1.0 / 3 ;
        constexpr static const real_d NE = 1.0 / 3 ;
        constexpr static const real_d SW = 1.0 / 3 ;
        constexpr static const real_d SE = 1.0 / 3 ;

        };
        
        class FWRestrictionStencil {
        public:
            
            constexpr static const real_d C = 0.25 ;
            constexpr static const real_d N = 0.125 ;
            constexpr static const real_d S = 0.125 ;
            constexpr static const real_d E = 0.125 ;
            constexpr static const real_d W = 0.125 ;
            constexpr static const real_d NW = 0.0625 ;
            constexpr static const real_d NE = 0.0625 ;
            constexpr static const real_d SW = 0.0625 ;
            constexpr static const real_d SE = 0.0625 ;
            
            FWRestrictionStencil(){} ;
            ~FWRestrictionStencil(){} ;
            
        };
        
        class BiLinInterPolStencil {
        public:
            
            constexpr static const real_d C = 1.0 ;
            constexpr static const real_d N = 0.5 ;
            constexpr static const real_d S = 0.5 ;
            constexpr static const real_d E = 0.5 ;
            constexpr static const real_d W = 0.5 ;
            constexpr static const real_d NW = 0.25 ;
            constexpr static const real_d NE = 0.25 ;
            constexpr static const real_d SW = 0.25 ;
            constexpr static const real_d SE = 0.25 ;

            
            BiLinInterPolStencil(){} ;
            ~BiLinInterPolStencil(){} ;
            
            
        };
    }
}

#endif /* Stencil_hpp */

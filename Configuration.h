#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include "Types.h"
#include "VcycleFormation.hpp"
#include <cmath>
#include <string>

using namespace SIWIR2::MULTIGRID ;

namespace SIWIR2 {

namespace MULTIGRID {

    class Configuration {

    private:

        real_i finestLevel_ ;
        real_i numberOfPreSmoothners_ ;
        real_i numberOfPostSmoothners_;
        real_i numberOfVcycles_ ;
        std::string  boundaryConditions_ ;


    public:


        // Constructors
        Configuration(int argc,char *argv[]) ;
        Configuration() ;

        // Destructors
        ~Configuration() ;

        // Member Functions
        real_l FinestLevel() ;
        real_l NumberOfPreSmoothners() ;
        real_l NumberOfPostSmoothners() ;
        real_l numberOfVcycles() ;
        BOUNDARY boundaryConditions() ;

        // Check Functions
        bool checkParameters() ; // false if any parameters is negative
    };

}
}

#endif // CONFIGURATION_H

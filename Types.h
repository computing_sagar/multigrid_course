//
//  Types.h
//  Assignment_1
//
//  Created by Sagar Dolas on 05/05/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#ifndef Types_h
#define Types_h

typedef double real_d ;
typedef float  real_f ;
typedef unsigned long real_l;
typedef int real_i ;
typedef long double real_ld ;

#endif /* Types_h */

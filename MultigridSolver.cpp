//
//  MultigridSolver.cpp
//  Assignment_1
//
//  Created by Sagar Dolas on 25/04/16.
//  Copyright © 2016 Sagar Dolas. All rights reserved.
//

#include "MultigridSolver.hpp"

using namespace SIWIR2::MULTIGRID ;

SIWIR2::MULTIGRID::Solver::Solver(const real_l _numTotalLevel, const real_l _numOfPresmths, const real_l _numOfPostsmths, const real_l _numVcycle, const SIWIR2::MULTIGRID::BOUNDARY _boundaryType) : SIWIR2::MULTIGRID::VcycleFormation(_numTotalLevel,_boundaryType), numVcycle_(_numVcycle), numPreSmoothners_(_numOfPresmths),numPostSmoothners_(_numOfPostsmths) {
    
    // Instantiating the stencil Objects
    SIWIR2::MULTIGRID::Stencil s ;
    SIWIR2::MULTIGRID::FWRestrictionStencil fw ;
    SIWIR2::MULTIGRID::BiLinInterPolStencil bi ; 
}

SIWIR2::MULTIGRID::Solver::~Solver(){
    // Destructor
}

void SIWIR2::MULTIGRID::Solver::start_Solver(){
    
    // Iterative VcycleLoop
    Initialise_F();
    for (real_l level = static_cast<real_l>(this->levelfine_) ; level > 1; --level) {
        // Relaxation
        for (real_l presm =0 ; presm < numPreSmoothners_; ++presm) {
            this->rb_Gauss_Siedel_Smoother(level,SIWIR2::MULTIGRID::GridType::Ugrid) ;
            if ((level == levelfine_) && boundaryType_==SIWIR2::MULTIGRID::BOUNDARY::Neumann) {
                update_Ghost_Points(-1.0) ;
            }
            else if(boundaryType_==SIWIR2::MULTIGRID::BOUNDARY::Neumann) {
                update_Ghost_Points(0.0);
            }
        }
        this->update_F(level) ;
        //Restriction
        this->full_Weighted_Restriction(level,level - 1 ) ;
    }
    // relaxation for coarsest grid
    this->rb_Gauss_Siedel_Smoother(1, SIWIR2::MULTIGRID::GridType::Ugrid) ;
    this->Bi_Lin_Interpolation(1, 2) ;

    // Coarse Grid Correction
    for (real_l level = 2; level <this->levelfine_; ++level) {
        //Relaxation
        for (real_l postsm = 0; postsm < numPostSmoothners_; ++postsm) {
            this->rb_Gauss_Siedel_Smoother(level,SIWIR2::MULTIGRID::GridType::Ugrid) ;
        }
        //Coarse Grid Correction--Bi_ linear interpolation
        this->Bi_Lin_Interpolation(level, level+1) ;
        
    }
    this->rb_Gauss_Siedel_Smoother(this->levelfine_, SIWIR2::MULTIGRID::GridType::Ugrid) ;
    if (boundaryType_==SIWIR2::MULTIGRID::Neumann) {
        update_Ghost_Points(-1.0) ;
    }
}

void SIWIR2::MULTIGRID::Solver::rb_Gauss_Siedel_Smoother( const real_l _level, const SIWIR2::MULTIGRID::GridType _gridtype){
    
    //*************************************************//
    // ALWAYS SMOOTHS U_ VECTOR FOR ANY LEVEL  ********//
    // INPUT = LEVEL and TYPE OF GRID TO SMOOTH *******//
    //*************************************************//
    
    // Calculate POS= position of corresonding Grid Object in Vector of Objects
    assert(_gridtype == 0) ;
    auto pos = static_cast<real_l>(this->get_Position(_level)) ;
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;
    auto h = 1.0 / (1<<_level) ;
    auto hsq = h * h ;
    real_d hsqinv = 1.0 / hsq ;

    real_l sigmafact = std::pow(2, pos) ;
    
    // Iterating over all Red Pos and Updating them
    #pragma omp parallel for schedule (static)
    for (real_l y = 1 ; y < N_y-1; ++y) {
        if (y & 1) { 
            for (real_l x = 1; x < N_x-1; x+=2) {
                //std::cout<<y * N + x <<std::endl;

                U_[pos]->operator()(x,y) =( F_[pos]->operator()(x,y) + ((five_Point_Update(pos,x,y)) * hsqinv) ) / ((Stencil::C * hsqinv)+ ( sigma->operator() ( x * sigmafact,y * sigmafact) ) ) ;
            }
        }
        else{
            for (real_l x = 2; x < N_x-1; x+=2) {
                //std::cout<<y * N + x <<std::endl;
                U_[pos]->operator()(x,y) = ( F_[pos]->operator()(x,y) + ((five_Point_Update(pos,x,y)) * hsqinv) ) / ((Stencil::C * hsqinv)+ ( sigma->operator() ( x * sigmafact,y * sigmafact) ) ) ;
            }
        }
    }
    
    // Iterating over black points and updating them
    #pragma omp parallel for schedule (static)
    for (real_l y =1; y < N_y-1; ++y) {
        if (y & 1) {
            for (real_l x =2; x < N_x-1; x+=2) {
                U_[pos]->operator()(x,y) = ( F_[pos]->operator()(x,y) + ((five_Point_Update(pos,x,y)) * hsqinv) ) / ((Stencil::C * hsqinv)+ ( sigma->operator() ( x * sigmafact,y * sigmafact) ) ) ;
            }
        }
        else{
            for (real_l x=1; x < N_x-1; x+=2) {
                U_[pos]->operator()(x,y) = ( F_[pos]->operator()(x,y) + ((five_Point_Update(pos,x,y)) * hsqinv) ) / ((Stencil::C * hsqinv)+ ( sigma->operator() ( x * sigmafact,y * sigmafact) ) ) ;

            }
        }
    }
}

void SIWIR2::MULTIGRID::Solver::update_F(const real_l _level){
    
    auto pos = static_cast<real_l>(this->get_Position(_level)) ;


    real_l sigmafact = std::pow(2, pos) ;
    //auto N = this->U_.at(pos)->get_Num_Grids() ;
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;

    auto h = 1.0 / (1<<_level) ;
    auto hsq = h * h ;
    auto hsqinv = 1.0 / hsq ;

    // Updating F as right hand for the residual
    #pragma omp parallel for schedule(static)
    for (real_l y = 1; y < N_y-1; ++y) {
        for (real_l x=1; x < N_x-1; ++x) {
            F_[pos]->operator()(x, y) =  (F_[pos]->operator()(x, y) + (( five_Point_Update(pos,x,y) - Stencil::C * U_[pos]->operator()(x,y) )* hsqinv) - ((sigma->operator()(x* sigmafact,y*sigmafact) ) * U_[0]->operator() (x,y)));
        }
    }
}

void SIWIR2::MULTIGRID::Solver::full_Weighted_Restriction(const real_l _finerLevel, const real_l _coarserLevel){
    
    //******************************************************
    //** FULL WEIGHTED RESTRICTION OPERATOR ****************
    //** INPUT = FINER_LEVEL AND COARSER_LEVEL *************
    //** USES NINE POINT STENCIL OPERATOR ******************
    //** RESIDUAL FOR THE COARSE GRID IS UPDATED************
    //** F_ IS UPDATED IMPLICILTY **************************
    //******************************************************
    
    // Calculating the Position and Number of Grid Points
    real_l pos = static_cast<real_l>(get_Position(_coarserLevel)) ;
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;
    real_l offset = 0 ;
    
    if (boundaryType_==SIWIR2::MULTIGRID::Neumann) {
        offset = 1 ;
    }
    // Iterating over all Grid-Points in Coarser Grid to get Updated from Finer Grid
    #pragma omp parallel for schedule (static)
    for (real_l y = 1; y< N_y-1 ; ++y) {
        for (real_l x =1; x < N_x-1 ; ++x) {
            // Updating the residual ...//
            F_[pos]->operator()(x, y) = nine_Point_Update(_finerLevel, 2*x-offset , 2*y, SIWIR2::MULTIGRID::ninePointUpdate::Restriction) ;
        }
    }
}

void SIWIR2::MULTIGRID::Solver::Bi_Lin_Interpolation(const real_l _coarserLevel, const real_l _finerLevel){
    
    // Bilinear Interpolation
    real_l pos = static_cast<real_l>(get_Position(_coarserLevel)) ;

    real_l offset = 0 ;
    if (boundaryType_==SIWIR2::MULTIGRID::Neumann) {
        offset = 1 ;
    }
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;
    for (real_l y = 1; y < N_y-1 ; ++y) {
        for (real_l x = 1; x < N_x-1 ; ++x) {
            
            auto updatestatus_ = nine_Point_Update(_finerLevel, (2*x)-offset, 2*y, SIWIR2::MULTIGRID::ninePointUpdate::Interpolation) ;
            if(updatestatus_==-1) std::cout<<"There is Something Wrong"<<std::endl;
        }
    } 
}

const real_d SIWIR2::MULTIGRID::Solver::five_Point_Update(const real_l pos, const real_l x, const real_l y){
    
    // Five Point Stencil Updation

    //auto N = this->U_.at(pos)->get_Num_Grids() ;
    return ( (Stencil::W * U_[pos]->operator()(x-1, y) +
              Stencil::E * U_[pos]->operator()(x+1, y) +
              Stencil::S * U_[pos]->operator()(x, y-1) +
              Stencil::N * U_[pos]->operator()(x, y+1))) ;

}

const real_d SIWIR2::MULTIGRID::Solver::nine_Point_Update(const real_l level, const real_l x, const real_l y, const SIWIR2::MULTIGRID::ninePointUpdate _ninePointUpdate){
    
    auto pos = static_cast<real_l>(get_Position(level)) ;    
    if (_ninePointUpdate == SIWIR2::MULTIGRID::ninePointUpdate::Restriction){
        
        // Restriction pos is position in finer level
        return ((FWRestrictionStencil::C  * F_[pos]->operator()(x, y)     +
                 FWRestrictionStencil::E  * F_[pos]->operator()(x+1, y)   +
                 FWRestrictionStencil::W  * F_[pos]->operator()(x-1, y)   +
                 FWRestrictionStencil::N  * F_[pos]->operator()(x, y+1)   +
                 FWRestrictionStencil::S  * F_[pos]->operator()(x, y-1)   +
                 FWRestrictionStencil::NE * F_[pos]->operator()(x+1, y+1) +
                 FWRestrictionStencil::NW * F_[pos]->operator()(x-1, y+1) +
                 FWRestrictionStencil::SW * F_[pos]->operator()(x-1, y-1) +
                 FWRestrictionStencil::SE * F_[pos]->operator()(x+1, y-1)) );
        
    }
    if(_ninePointUpdate==SIWIR2::MULTIGRID::ninePointUpdate::Interpolation){
        
        // Interpolation
        real_l offset = 0;
        
        if (boundaryType_==SIWIR2::MULTIGRID::BOUNDARY::Neumann) {
            offset = 1 ;
        }
        auto pos_c =  static_cast<real_l>(get_Position( level - 1)) ;
        auto x_c = (x+offset)>>1 ;
        auto y_c = y>>1 ;
        
        U_[pos]->operator()(x,y)     += BiLinInterPolStencil::C  *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x-1,y)   += BiLinInterPolStencil::W  *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x+1,y)   += BiLinInterPolStencil::E  *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x,y+1)   += BiLinInterPolStencil::N  *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x+1,y+1) += BiLinInterPolStencil::NE *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x-1,y+1) += BiLinInterPolStencil::NW *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x-1,y-1) += BiLinInterPolStencil::SW *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x,y-1)   += BiLinInterPolStencil::S  *  U_[pos_c]->operator()(x_c,y_c) ;
        U_[pos]->operator()(x+1,y-1) += BiLinInterPolStencil::SE *  U_[pos_c]->operator()(x_c,y_c) ;
        
        return 1 ;
    }
    else
        return -1 ;
}

void SIWIR2::MULTIGRID::Solver::calculate_Residual(const real_l _level){
    
    auto pos = static_cast<real_l>(get_Position(_level)) ;
    //auto N = U_[pos]->get_Num_Grids() ;
    
    auto N_x = this->U_[pos]->getNx() ;
    auto N_y = this->U_[pos]->getNy() ;
    auto h = 1.0 / (1<<_level) ;
    auto hsq = h * h ;
    auto hsqinv = 1.0 / hsq ;

    // Calculating residual for inner points //
    #pragma omp parallel for schedule (static)
    for (real_l y =1;y < N_y-1 ; ++y ) {
        for (real_l x= 1; x < N_x-1; ++x) {
            R_[pos]->operator()(x,y) = (F_[pos]->operator()(x, y) + (hsqinv* (five_Point_Update(pos, x, y) - (real_d(4.0) * U_[pos]->operator()(x, y))))) ;
            //std::cout<<R_[pos]->operator()(x, y)<<std::endl ;
        }
    }
}


void SIWIR2::MULTIGRID::Solver::start_Simulation(){
    
    real_d time_taken = 0.0 ;
    // SOLVER FLOW DESIGN
    SIWIR2::Timer timePoint ;
    Initialise_F();
    for(real_l i = 0 ; i < numVcycle_ ; ++i){
        timePoint.reset();
        start_Solver();
        time_taken += timePoint.elapsed() ;
        //reset_F();
        Initialise_F();
        reset_U();
        //calculate_Residual(levelfine_);
        //calculate_Residual_Norm();
        //std::cout<<"The Residual for "<<i+1<<" "<<"Vcycle"<<" is:="<<residualarray_[i]<<std::endl;
        //std::cout<<"The Convergence rate is:= "<<convergence_Rate(i)<<std::endl;
    }
    std::cout<<"The total time taken for "<<numVcycle_<<" "<<"Vycles are "<<time_taken<<" milliseconds"<<std::endl ;
    std::cout<<"The average time taken for each Vcycle is "<< time_taken / numVcycle_ <<" milliseconds"<<std::endl ;
}

// Function for Residual Norm
void SIWIR2::MULTIGRID::Solver::calculate_Residual_Norm(){

    auto N_x = this->U_[0]->getNx() ;
    auto N_y = this->U_[0]->getNy() ;

    real_l interPoints_ = (N_x) * (N_y) ;
    real_d innerProduct_ = 0.0 ; //= std::inner_product(R_[0]->getbegin(),R_[0]->getlast(),R_[0]->getbegin(),0.0 ) ;

    for(real_l y = 0 ; y < N_y-1;++y){
        for(real_l x =0 ; x < N_x-1 ; ++x){
            innerProduct_ += R_[0]->operator()(x,y) * R_[0]->operator()(x,y) ;
        }
    }
    innerProduct_ /= interPoints_  ;
    real_ld norm = std::sqrt(innerProduct_) ;
    residualarray_.push_back(norm) ;
}

// Residual for Compute Error
const real_d SIWIR2::MULTIGRID::Solver::compute_Error(){
    
    auto N = U_[0]->get_Num_Grids() ;
    auto meshsize_ = 1.0 / (N-1) ;

    auto N_x = this->U_[0]->getNx() ;
    auto N_y = this->U_[0]->getNy() ;

    real_l interPoints_ = (N_x ) * (N_y) ;
    #pragma omp parallel for schedule (static)
    for (real_l y =0 ; y < N; ++y) {
        for (real_l x =0 ; x < N; ++x ) {
                real_d xcord_ = x * meshsize_ ;
                real_d ycord_ = y * meshsize_ ;
                real_ld error = this->U_.at(0)->operator()(x, y) -((sin(SIWIR2::pie * xcord_ ) * sinh(SIWIR2::pie * ycord_))) ;
                //std::cout<<error<<std::endl;
                error_.push_back(error) ;
        }
    }

    real_d inner_product = std::inner_product(error_.begin(),error_.end(),error_.begin(),0.0) ;
    return std::sqrt(inner_product / interPoints_ ) ;
}

// Convergence Rate Calculation
const real_d SIWIR2::MULTIGRID::Solver::convergence_Rate(real_l index_){
    if(index_>0)
        return  (residualarray_[index_] / residualarray_[index_-1]) ;
    else
        return  0.0 ;
}

void SIWIR2::MULTIGRID::Solver::reset_F(){

    real_d f_value = 0.0 ;
    
    if(boundaryType_==SIWIR2::MULTIGRID::BOUNDARY::Neumann) {
        f_value= 2.0 ;
    }

    auto N_x = this->U_[0]->getNx() ;
    auto N_y = this->U_[0]->getNy() ;
    #pragma omp parallel for schedule (static)
    for (real_l y =0; y <N_y ; ++y ) { // made changes in the direction of
        for (real_l x = 1;x < N_x-1 ; ++x) {
            F_[0]->operator()(x, y) = f_value ;
        }
    }
}

void SIWIR2::MULTIGRID::Solver::reset_U(){
    
    for (real_l iter = 1; iter< U_.size(); ++iter) {
        if (boundaryType_==SIWIR2::MULTIGRID::BOUNDARY::Dirichlet) {
            U_[iter]->reset() ;
        }
        else {
            U_[iter]->resize() ; 
        }
     
    }   
}










